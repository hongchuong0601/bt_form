import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormActions } from "../storeToolkit/BTForm/slice";

const TableSinhVien = () => {
  const { svList } = useSelector((state) => state.btForm);
  console.log("svList:", svList);

  const dispatch = useDispatch();

  return (
    <div className="mt-3">
      <table className="table">
        <thead>
          <tr>
            <th>Mã SV</th>
            <th>Họ tên</th>
            <th>Số ĐT</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          {svList?.map((prd) => (
            <tr key={prd?.maSV}>
              <td>{prd?.maSV}</td>
              <td>{prd?.hoTen}</td>
              <td>{prd?.soDT}</td>
              <td>{prd?.email}</td>
              <td>
                <button
                  className="btn btn-warning"
                  onClick={() => {
                    dispatch(btFormActions.deleteSinhVien(prd.maSV));
                  }}
                >
                  Delete
                </button>
                <button
                  className="btn btn-secondary ml-2"
                  onClick={() => {
                    dispatch(btFormActions.editSinhVien(prd));
                  }}
                >
                  Edit
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TableSinhVien;
