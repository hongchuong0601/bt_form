import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { btFormActions } from "../storeToolkit/BTForm/slice";

const FormSinhVien = () => {
  const [formData, setFormData] = useState();
  console.log("formData:", formData);

  const [formError, setFormError] = useState();
  console.log("formError:", formError);
  const dispatch = useDispatch();

  const { svEdit } = useSelector((state) => state.btForm);
  console.log("svEdit:", svEdit);

  const handleFormData = () => (ev) => {
    const { name, value, title, minLength, max, pattern, validity } = ev.target;
    console.log("pattern:", pattern);
    console.log("validity:", validity);
    console.log("minLength:", minLength);

    let mess;

    if (minLength !== -1 && !value.length) {
      mess = `Vui lòng nhập thông tin ${title}`;
    } else if (value.length < minLength) {
      mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`;
    } else if (value.length > max && max) {
      mess = `Chỉ được nhập tối đa ${max} ký tự`;
    } else if (validity.patternMismatch && ["maSV", "soDT"].includes(name)) {
      mess = `Vui lòng nhập ký tự là số`;
    } else if (validity.patternMismatch && name === "email") {
      mess = `Vui lòng nhập đúng email`;
    }

    setFormError({
      ...formError,
      [name]: mess,
    });

    setFormData({
      ...formData,
      [name]: mess ? undefined : value,
    });
  };

  useEffect(() => {
    if (!svEdit) return;
    setFormData(svEdit);
  }, [svEdit]);

  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        console.log("submit");

        const elements = document.querySelectorAll("input");
        console.log("elements:", elements);

        let formError = {};

        elements.forEach((ele) => {
          let mess;

          const { name, value, title, minLength, max, pattern, validity } = ele;

          if (minLength !== -1 && !value.length) {
            mess = `Vui lòng nhập thông tin ${title}`;
          } else if (value.length < minLength) {
            mess = `Vui lòng nhập tối thiểu ${minLength} ký tự`;
          } else if (value.length > max && max) {
            mess = `Chỉ được nhập tối đa ${max} ký tự`;
          } else if (
            validity.patternMismatch &&
            ["maSV", "soDT"].includes(name)
          ) {
            mess = `Vui lòng nhập ký tự là số`;
          } else if (validity.patternMismatch && name === "email") {
            mess = `Vui lòng nhập đúng email`;
          }

          formError[name] = mess;
        });

        console.log("formError:", formError);

        let flag = false;

        for (let key in formError) {
          if (formError[key]) {
            flag = true;
            break;
          }
        }
        if (flag) {
          setFormError(formError);
          return;
        }

        if (svEdit) {
          dispatch(btFormActions.updateSinhVien(formData));
        } else {
          dispatch(btFormActions.addSinhVien(formData));
        }
      }}
      noValidate
    >
      <h2 className="px-2 py-2 bg-dark text-warning">Thông tin sinh viên</h2>
      <div className="form-group row">
        <div className="col-6">
          <p>Mã SV</p>
          <input
            type="text"
            disabled={!!svEdit}
            value={formData?.maSV}
            className="form-control"
            name="maSV"
            title="maSV"
            minLength={6}
            max={20}
            pattern="^[0-9]+$"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.maSV}</p>
        </div>
        <div className="col-6">
          <p>Họ tên</p>
          <input
            type="text"
            value={formData?.hoTen}
            className="form-control"
            name="hoTen"
            title="hoTen"
            minLength={1}
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.hoTen}</p>
        </div>
      </div>
      <div className="form-group row">
        <div className="col-6">
          <p>Số điện thoại</p>
          <input
            type="text"
            value={formData?.soDT}
            className="form-control"
            name="soDT"
            minLength={9}
            pattern="^[0-9]+$"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.soDT}</p>
        </div>
        <div className="col-6">
          <p>Email</p>
          <input
            type="text"
            pattern="[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,}$"
            className="form-control"
            name="email"
            onChange={handleFormData()}
          />
          <p className="text-danger">{formError?.email}</p>
        </div>
      </div>
      <div className="mt-3">
        {svEdit && (
          <button className="btn btn-success ml-3">Cập nhật sinh viên</button>
        )}
        {!svEdit && (
          <button className="btn btn-primary ">Thêm sinh viên</button>
        )}
      </div>
    </form>
  );
};

export default FormSinhVien;
