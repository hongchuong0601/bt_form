import React from "react";

import FormSinhVien from "./FormSinhVien";
import TableSinhVien from "./TableSinhVien";

const BTSinhVien = () => {
  return (
    <div className="container">
      <h1>BTSinhVien</h1>
      <FormSinhVien />
      <TableSinhVien />
    </div>
  );
};

export default BTSinhVien;
