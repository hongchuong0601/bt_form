import logo from "./logo.svg";
import "./App.css";

import BTSinhVien from "./BTForm/BTSinhVien";

function App() {
  return (
    <div className="App">
      <BTSinhVien />
    </div>
  );
}

export default App;
