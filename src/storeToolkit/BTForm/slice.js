import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  svList: [],
  svEdit: undefined,
};

const btFormSlice = createSlice({
  name: "btForm",
  initialState,
  reducers: {
    addSinhVien: (state, action) => {
      state.svList.push(action.payload);
    },

    deleteSinhVien: (state, action) => {
      state.svList = state.svList.filter((prd) => prd.maSV !== action.payload);
    },

    editSinhVien: (state, action) => {
      state.svEdit = action.payload;
    },

    updateSinhVien: (state, action) => {
      state.svList = state.svList.map((prd) => {
        if (prd.maSV === action.payload.maSV) {
          return action.payload;
        }
        return prd;
      });

      // sau khi update thành công thì chuyển giá trị svEdit thành undefined
      state.svEdit = undefined;
    },
  },
});

export const { actions: btFormActions, reducer: btFormReducer } = btFormSlice;
