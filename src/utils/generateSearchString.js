/**
 *
 * @param {*} object
 * @returns - search string dùng để query URL
 */

// js docs

import qs from "qs";

export const generateSearchString = (params) => {
  const searchString = qs.stringify(params, {
    addQueryPrefix: true,
  });

  return searchString;
};
